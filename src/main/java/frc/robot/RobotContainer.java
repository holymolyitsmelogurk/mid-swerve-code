// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.File;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Commands;
import frc.robot.commands.swerve.FieldDrive;
import frc.robot.commands.swerve.FieldDrive2;
import frc.robot.subsystems.LogModule;
import frc.robot.subsystems.SwerveSubsystem;
import swervelib.math.SwerveMath;

public class RobotContainer {
  // LogModule mod_1 = new LogModule(1, 2, 0, 1);
  // LogModule mod_2 = new LogModule(3, 4, 1, 2);
  // LogModule mod_3 = new LogModule(5, 6, 2, 3);
  // LogModule mod_4 = new LogModule(7, 8, 3, 4);
  
  private final SwerveSubsystem drivebase = new SwerveSubsystem(
    new File(
      Filesystem.getDeployDirectory(), 
      "swerve/"
    )
  );

  public static XboxController driverController = new XboxController(0);

  public RobotContainer() {
    configureBindings();
    setupSwerve();
    // SwerveMath.calculateDegreesPerSteeringRotation(21.4285714286, 1)
  }

  /**
   * gloop mode activation procedure 
   */
  public void setupSwerve() {
    // vvvvv actually dog vvvvvv
    //
    // FieldDrive teleop = new FieldDrive(drivebase, 
    //   () -> MathUtil.applyDeadband( //xIn
    //     driverController.getLeftX(), 
    //     0.01
    //   ), 
    //   () -> MathUtil.applyDeadband( //yIn
    //     driverController.getLeftY(), 
    //     0.01
    //   ), 
    //   () -> -driverController.getRightX(), //xHead
    //   () -> -driverController.getRightY()  //yHead
    // );

    FieldDrive2 teleop = new FieldDrive2(
      drivebase, 
      () -> MathUtil.applyDeadband(
        driverController.getLeftX(), 
        0.01
      ), 
      () -> MathUtil.applyDeadband(
        driverController.getLeftY(), 
        0.01
      ), 
      () -> MathUtil.applyDeadband(
        driverController.getRightX(), 
        0.01
      )
    );
    
    drivebase.setDefaultCommand(teleop);
  }

  private void configureBindings() {}

  public Command getAutonomousCommand() {
    return Commands.print("No autonomous command configured");
  }
}
