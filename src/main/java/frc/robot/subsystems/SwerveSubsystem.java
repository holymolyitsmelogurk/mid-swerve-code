// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.io.File;


import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotContainer;
import swervelib.SwerveController;
import swervelib.SwerveDrive;
import swervelib.SwerveModule;
import swervelib.parser.SwerveControllerConfiguration;
import swervelib.parser.SwerveDriveConfiguration;
import swervelib.parser.SwerveParser;
import swervelib.telemetry.SwerveDriveTelemetry;
import swervelib.telemetry.SwerveDriveTelemetry.TelemetryVerbosity;

public class SwerveSubsystem extends SubsystemBase {
  private final SwerveDrive swervedrive;
  public final double maxSpeed = Units.feetToMeters(14.5);

  /** Creates a new Swervedrive. */
  public SwerveSubsystem(File dir) {
    SwerveDriveTelemetry.verbosity = TelemetryVerbosity.HIGH;

    try {
      swervedrive = new SwerveParser(dir).createSwerveDrive(maxSpeed);
    } catch ( Exception e) {
      throw new RuntimeException(e);
    }

    swervedrive.setHeadingCorrection(false);
  }

  /**
   * make a swerve  (dont use this)
   * @param driveCfg cfg #1
   * @param controllerCfg #2
   */
  public SwerveSubsystem(SwerveDriveConfiguration driveCfg, SwerveControllerConfiguration controllerCfg) {
    swervedrive = new SwerveDrive(driveCfg, controllerCfg, maxSpeed);
  }

  /**
   * go, harder!!!!!! (this is the one that is used in fieldrive)
   * @param trans vectoré
   * @param rot rotat
   * @param isFieldRel is it
   */
  public void drive(Translation2d trans, double rot, boolean isFieldRel) {
    swervedrive.drive(
      trans, 
      rot, 
      isFieldRel, 
      false
    );
  }

  /**
   * go, differently????
   * @param velo go
   */
  public void driveFieldOriented(ChassisSpeeds velo) {
    swervedrive.driveFieldOriented(velo);
  }

  /**
   * go????
   * @param velo go
   */
  public void drive(ChassisSpeeds velo) {
    swervedrive.drive(velo);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    // for (SwerveModule mod : swervedrive.getModules()) {
    //   SmartDashboard.putNumber("enc-pos-" + mod.moduleNumber, mod.getAbsolutePosition());
    // }

    SmartDashboard.putNumber("joyLX", MathUtil.applyDeadband( 
      RobotContainer.driverController.getLeftX(), 
      0.01
    ));

    SmartDashboard.putNumber("joyLY", MathUtil.applyDeadband( 
      RobotContainer.driverController.getLeftY(), 
      0.01
    ));
    SmartDashboard.putNumber("joyRX", RobotContainer.driverController.getRightX());
    SmartDashboard.putNumber("joyRY", RobotContainer.driverController.getRightY());

    for (SwerveModule mod : swervedrive.getModules()) {
      AnalogEncoder a = new AnalogEncoder((AnalogInput)mod.configuration.absoluteEncoder.getAbsoluteEncoder());
      
      SmartDashboard.putNumber("enc-pos-"+mod.moduleNumber, a.get());

      a.close();
    }
  }

  /**
   * 'matics
   * @return kinematics
   */
  public SwerveDriveKinematics getKinematics() {
    return swervedrive.kinematics;
  }

  /**
   * positione
   * @return whereabouts
   */
  public Pose2d getPose() {
    return swervedrive.getPose();
  }

  /**
   * rotation on the up axis 
   * @return spinning 
   */
  public Rotation2d getYaw() {
    return swervedrive.getYaw();
  }

  /**
   * literally just calls getYaw()
   */
  public Rotation2d getHeading() {
    return this.getYaw();
  }

  /**
   * how 2 drive based on joy inputse
   * 
   * @note add one for just 1 joy see example
   * 
   * @param xIn x axise (move)
   * @param yIn y axise  (move)
   * @param xHead x axlise (spin)
   * @param yHead y axlise (spin)
   * @return nyoom numbers. 
   */
  public ChassisSpeeds getTargetSpeeds(double xIn, double yIn, double xHead, double yHead) {
    xIn = Math.pow(xIn, 3);
    yIn = Math.pow(yIn, 3);
    return swervedrive.swerveController.getTargetSpeeds(xIn, yIn, xHead, yHead, getYaw().getRadians(), maxSpeed);
  }

  public ChassisSpeeds getFieldVelo() {
    return swervedrive.getFieldVelocity();
  }

  public ChassisSpeeds getRobotVelo() {
    return swervedrive.getRobotVelocity();
  }

  public SwerveController getController() {
    return swervedrive.swerveController;
  }

  public SwerveDriveConfiguration getCfg() {
    return swervedrive.swerveDriveConfiguration;
  }

  public void lock() {
    swervedrive.lockPose();
  }

  public Rotation2d getPitch() {
    return swervedrive.getPitch();
  }

  /**
   * make go raw
   * @param speeds go
   */
  public void setChassisSpeeds(ChassisSpeeds speeds) {
    swervedrive.setChassisSpeeds(speeds);
  }

  /**
   * breakmode
   * 
   * @param brake true if to break. false is wiggly!
   */
  public void setBrakemode(boolean brake) {
    swervedrive.setMotorIdleMode(brake);
  }

  /**
   * i dont know what this does. 
   * @param trajectory honestly got no clue!!!
   */
  public void postTrajectory(Trajectory trajectory) {
    swervedrive.postTrajectory(trajectory);
  }

  /**
   * dont use this it will ruin your day
   */
  public void resetOdometry() {
    resetOdometry(new Pose2d(
      new Translation2d(0,0),
      new Rotation2d(0)
    ));
  }

  /**
   * gyro is make zeroe.
   */
  public void resetGyro() {
    swervedrive.zeroGyro();
  }

  /**
   * position hallucination
   * @param initial gaslighting positional argument
   */
  public void resetOdometry(Pose2d initial) {
    swervedrive.resetOdometry(initial);
  }
}
