// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LogModule extends SubsystemBase {
  private CANSparkMax fwd_motor = null;
  private CANSparkMax rot_motor = null;
  private AnalogEncoder encoder = null;
  private int module_num = 0;

  /** Creates a new LogModule. */
  public LogModule(int fwd_id, int rot_id, int enc_id, int mod_id) {
    fwd_motor = new CANSparkMax(fwd_id, MotorType.kBrushless);
    fwd_motor.restoreFactoryDefaults();
    fwd_motor.setIdleMode(IdleMode.kCoast);

    rot_motor = new CANSparkMax(rot_id, MotorType.kBrushless);
    rot_motor.restoreFactoryDefaults();
    rot_motor.setIdleMode(IdleMode.kCoast);
    
    encoder = new AnalogEncoder(enc_id);
    module_num = mod_id;
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run

    SmartDashboard.putNumber("Swerve Module #" + module_num + " - Encoder/get", encoder.get());
    SmartDashboard.putNumber("Swerve Module #" + module_num + " - Encoder/absolue", encoder.getAbsolutePosition());
  }
}
