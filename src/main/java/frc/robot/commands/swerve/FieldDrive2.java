// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.swerve;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;
import swervelib.SwerveController;

public class FieldDrive2 extends CommandBase {
  private final SwerveSubsystem  swerve;
  private final DoubleSupplier   vX;
  private final DoubleSupplier   vY;
  private final DoubleSupplier   omega;
  private final SwerveController controller;
  
  /** Creates a new FieldDrive2. */
  public FieldDrive2(
    SwerveSubsystem swerve, 
    DoubleSupplier vX, 
    DoubleSupplier vY, 
    DoubleSupplier omega
  ) {
    this.swerve = swerve;
    this.vX = vX;
    this.vY = vY;
    this.omega = omega;
    this.controller = swerve.getController();
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double xVelo   = Math.pow(vX.getAsDouble(), 3);
    double yVelo   = Math.pow(vY.getAsDouble(), 3);
    double aVelo   = Math.pow(omega.getAsDouble(), 3);

    SmartDashboard.putNumber("vX", xVelo);
    SmartDashboard.putNumber("vY", yVelo);
    SmartDashboard.putNumber("omega", aVelo);

    swerve.drive(
      new Translation2d(
        xVelo * swerve.maxSpeed,
        yVelo * swerve.maxSpeed
      ), 
      aVelo * controller.config.maxAngularVelocity, 
      true
    );
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
