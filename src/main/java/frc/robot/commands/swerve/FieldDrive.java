// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.swerve;

import java.util.List;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.SwerveSubsystem;
import swervelib.SwerveController;
import swervelib.SwerveDrive;
import swervelib.math.SwerveMath;

public class FieldDrive extends CommandBase {
  private final SwerveSubsystem swervedrive;
  private final DoubleSupplier  xIn, yIn, xHead, yHead;
  private boolean initRotation = true;


  /** Creates a new AbsFieldDrive. */
  public FieldDrive(
    SwerveSubsystem swervedrive,
    DoubleSupplier xIn, 
    DoubleSupplier yIn, 
    DoubleSupplier xHead,
    DoubleSupplier yHead
  ) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.swervedrive = swervedrive;
    this.xIn = xIn;
    this.yIn = yIn;
    this.xHead = xHead;
    this.yHead = yHead;
    
    addRequirements(this.swervedrive);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    initRotation = true;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ChassisSpeeds desired = swervedrive.getTargetSpeeds(
      xIn.getAsDouble(), 
      yIn.getAsDouble(),
      xHead.getAsDouble(),
      yHead.getAsDouble()
    ); 

    // Prevent Movement After Auto
    if(initRotation)
    {
      if(xHead.getAsDouble() == 0 && yHead.getAsDouble() == 0)
      {
        // Get the curretHeading
        double firstLoopHeading = swervedrive.getHeading().getRadians();
      
        // Set the Current Heading to the desired Heading
        desired = swervedrive.getTargetSpeeds(0, 0, Math.sin(firstLoopHeading), Math.cos(firstLoopHeading));
      }
      //Dont Init Rotation Again
      initRotation = false;
    }

    // NO velocity limit frfr (i dont know what the mass of the robot is)
    Translation2d trans = SwerveController.getTranslation2d(desired);
    // trans = SwerveMath.limitVelocity(trans, swervedrive.getFieldVelo(), swervedrive.getPose(), 0.13, 0, List.of(Const), null)

    swervedrive.drive(trans, desired.omegaRadiansPerSecond, true);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
